﻿namespace Pandora
{
    public enum ResourceType
    {
        DOUGH = 10,
        BUTTER = 11,
        BREAD = 20,
        RAW_CROISSANT = 25,
        CROISSANT = 30,
        FLOUR = 2,
        WATER = 3,
    }
}