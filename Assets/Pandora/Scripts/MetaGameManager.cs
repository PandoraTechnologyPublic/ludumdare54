﻿using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;
using UnityEngine.SceneManagement;

namespace Pandora
{
    public class MetaGameManager : MonoBehaviour
    {
        [SerializeField] private TMP_Text _blinkingText;

        private void Start()
        {
            // InputSystem.onAnyButtonPress.Subscribe(Test());
            InputSystem.onAnyButtonPress
                .CallOnce(ctrl => SceneManager.LoadScene(1, LoadSceneMode.Single));
            // InputSystem.onActionChange += OnInputSystemOnonActionChange;

            MakeBlink();
        }

        private void OnInputSystemOnonActionChange(object obj, InputActionChange change)
        {
            // obj can be either an InputAction or an InputActionMap
            // depending on the specific change.
            switch (change)
            {
                case InputActionChange.ActionStarted:
                case InputActionChange.ActionPerformed:
                case InputActionChange.ActionCanceled:
                {
                    InputAction inputAction = ((InputAction)obj);

                    if (new List<string>{"Submit", "Click"}.Contains(inputAction.name))
                    {
                        InputSystem.onActionChange -= OnInputSystemOnonActionChange;
                        SceneManager.LoadScene(1, LoadSceneMode.Single);
                    }

                    // Debug.Log($"{inputAction.name} {change}");
                    break;
                }
            }
        }

        private void MakeBlink()
        {
            DOTween.Sequence()
                .Append(_blinkingText.DOFade(0, 1))
                .Append(_blinkingText.DOFade(1, 1))
                .AppendCallback(MakeBlink);
        }
    }
}