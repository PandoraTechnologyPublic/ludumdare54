﻿using System;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Pandora
{
    public class CustomerComponent : MonoBehaviour
    {
        [SerializeField] private float _waitTime = 30f;
        [SerializeField] private Image _icon;
        [SerializeField] private Transform _coinIcon;
        [SerializeField] private Transform _happyIcon;
        [SerializeField] private Transform _sadIcon;
        [SerializeField] private GameObject _mesh;
        [SerializeField] private List<GameObject> _meshPrefabs;

        private ResourceType _need;
        private NavMeshAgent _navMeshAgent;
        private GameManager _gameManager;
        private ResourceManager _resourceManager;
        private AudioManager _audioManager;
        private TimerComponent _timer;
        private Animator _animator;
        // private GameObject _newmesh;

        private float _currentWaitTime;
        private float _startSpeed;
        private float _startAngularSpeed;
        private Transform _counter;

        private void Awake()
        {
            // Tentative de swap du mesh - Nope!
            Transform parent = _mesh.transform.parent;
            GameObject newMesh = GameObject.Instantiate(_meshPrefabs[Random.Range(0, _meshPrefabs.Count)]);
            newMesh.transform.SetParent(parent, false);
            newMesh.transform.rotation = Quaternion.Euler(0, 180, 0);

            _animator = newMesh.GetComponent<Animator>();

            _resourceManager = GameObject.FindAnyObjectByType<ResourceManager>();
            _timer = GetComponentInChildren<TimerComponent>();

            GameObject.Destroy(_mesh.gameObject);
        }

        private void Start()
        {
            _navMeshAgent = GetComponent<NavMeshAgent>();
            _gameManager = GameObject.FindAnyObjectByType<GameManager>();
            _audioManager = GameObject.FindAnyObjectByType<AudioManager>();
            // _animator = GetComponentInChildren<Animator>();

            // _navMeshAgent.SetDestination(_gameManager.GetRandomDestination().position);
            _startSpeed = _navMeshAgent.speed;
            _startAngularSpeed = _navMeshAgent.angularSpeed;

            if (_counter != null)
            {
                _navMeshAgent.SetDestination(_counter.position);
            }

            _coinIcon.gameObject.SetActive(false);
            _happyIcon.gameObject.SetActive(false);
            _sadIcon.gameObject.SetActive(false);
        }

        private void Update()
        {
            float timeDilation = _gameManager.GetTimeDilation();

            _navMeshAgent.speed = _startSpeed * timeDilation;
            _navMeshAgent.angularSpeed= _startAngularSpeed * timeDilation;
            _animator.SetFloat("timeDilation", timeDilation);

            if (!_navMeshAgent.pathPending && _navMeshAgent.remainingDistance < 0.5f)
            {
                _currentWaitTime -= Time.deltaTime * timeDilation;

                if (_counter != null)
                {
                    // :TODO: increase timer speed
                }
                else if (_currentWaitTime <= 0)
                {
                    // randomly wait at destination
                    _navMeshAgent.SetDestination(_gameManager.GetRandomDestination().position);
                    _currentWaitTime = Random.Range(3, 7);
                }
                _animator.SetBool("walking", false);
            }
            else
            {
                _animator.SetBool("walking", true); 
            }
        }

        public void Buy(ResourceType resourceType)
        {
            _gameManager.AddMoney(GameplayConfig.RESOURCE_COSTS[resourceType]);
            _gameManager.AddSatisfiedCustomer();
            // ResetNeed(); // Temp until customers goes away/disappear
            _timer.gameObject.SetActive(false);

            _coinIcon.localPosition = Vector3.zero;
            _coinIcon.gameObject.SetActive(true);

            DOTween.Sequence()
                .Append(_coinIcon.DOLocalMoveY(1, 0.3f))
                .AppendCallback(
                    () =>
                    {
                        _coinIcon.gameObject.SetActive(false);
                        _gameManager.QuitBakery(this);
                    });
        }

        public ResourceType GetNeedType()
        {
            return _need;
        }

        public void GoToCounter(Transform counter)
        {
            _counter = counter;

            if (_navMeshAgent != null)
            {
                _navMeshAgent.SetDestination(_counter.position);
            }
        }

        public void SetNeed(ResourceType customerNeed)
        {
            _need = customerNeed;

            _icon.sprite = _resourceManager.GetResourcePrefab(customerNeed).GetIcon();

            _timer.StartTimer(_waitTime, ()=>
            {
                _timer.gameObject.SetActive(false);

                _gameManager.AddUnsatisfiedCustomer();

                _sadIcon.localPosition = Vector3.zero;
                _sadIcon.gameObject.SetActive(true);

                _audioManager.PlaySound(_audioManager._customerUnhappy);

                DOTween.Sequence()
                    .Append(_sadIcon.DOLocalMoveY(1, 0.3f))
                    .AppendCallback(
                        () =>
                        {
                            _coinIcon.gameObject.SetActive(false);
                            _gameManager.QuitBakery(this);
                        });
            });
        }
    }
}