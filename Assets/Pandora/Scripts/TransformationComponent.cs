﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Pandora
{
    public class TransformationComponent : MonoBehaviour
    {
        [SerializeField] private List<ResourceType> _recipes;
        [SerializeField] private Transform _emptyTransformer;
        [SerializeField] private Transform _filledTransformer;
        [SerializeField] private Image _icon;

        private ResourceManager _resourceManager;
        private TimerComponent _timer;
        private TransformerState _currentState;
        private readonly List<ResourceType> _contentList = new List<ResourceType>();
        private readonly List<Recipe> _currentlyAvailableRecipes = new List<Recipe>();
        private AudioManager _audioManager;

        public enum TransformerState
        {
            EMPTY,
            WAITING_INGREDIENTS,
            IS_COOKING,
            COOKED,
            BURNED,
        }

        private void Start()
        {
            _resourceManager = GameObject.FindAnyObjectByType<ResourceManager>();
            _audioManager = GameObject.FindAnyObjectByType<AudioManager>();
            _timer = GetComponentInChildren<TimerComponent>();

            _emptyTransformer.gameObject.SetActive(true);
            _filledTransformer.gameObject.SetActive(false);

            ResetRecipes();
        }

        private void ResetRecipes()
        {
            _currentlyAvailableRecipes.Clear();

            foreach (Recipe recipe in GameplayConfig.RECIPES)
            {
                if (_recipes.Contains(recipe._output))
                {
                    _currentlyAvailableRecipes.Add(recipe);
                }
            }

            _contentList.Clear();

            _currentState = TransformerState.EMPTY;

            SetIconVisibility(false);
        }

        public bool DoesRequireIngredient(ResourceType inputIngredient)
        {
            bool doesRequireIngredient = false;

            foreach (Recipe currentlyAvailableRecipe in _currentlyAvailableRecipes)
            {
                if (currentlyAvailableRecipe._inputs.Contains(inputIngredient) && !_contentList.Contains(inputIngredient))
                {
                    doesRequireIngredient = true;
                }
            }

            return doesRequireIngredient;
        }

        private bool HasRecipe(ResourceType inputResource)
        {
            bool hasRecipe = false;

            foreach (Recipe currentlyAvailableRecipe in _currentlyAvailableRecipes)
            {
                if (currentlyAvailableRecipe._inputs.Contains(inputResource))
                {
                    hasRecipe = true;
                }
            }

            return hasRecipe;
        }

        public void AddContent(TransportableComponent transportableComponent)
        {
            _emptyTransformer.gameObject.SetActive(transportableComponent == null);
            _filledTransformer.gameObject.SetActive(transportableComponent != null);

            _timer.gameObject.SetActive(transportableComponent != null);

            if (transportableComponent != null)
            {
                for (int availableRecipeIndex = _currentlyAvailableRecipes.Count - 1; availableRecipeIndex >= 0; availableRecipeIndex--)
                {
                    Recipe currentlyAvailableRecipe = _currentlyAvailableRecipes[availableRecipeIndex];

                    if (!currentlyAvailableRecipe._inputs.Contains(transportableComponent.GetResourceType()))
                    {
                        _currentlyAvailableRecipes.RemoveAt(availableRecipeIndex);
                    }
                }

                _contentList.Add(transportableComponent.GetResourceType());
                _currentState = TransformerState.WAITING_INGREDIENTS;

                if (_currentlyAvailableRecipes.Count == 1)
                {
                    bool isRecipeComplete = true;
                    Recipe selectedRecipe = _currentlyAvailableRecipes[0];

                    foreach (ResourceType resourceType in selectedRecipe._inputs)
                    {
                        if (!_contentList.Contains(resourceType))
                        {
                            isRecipeComplete = false;
                        }
                    }

                    if (isRecipeComplete)
                    {
                        _currentState = TransformerState.IS_COOKING;

                        _timer.StartTimer(selectedRecipe._transformationDuration, () => OnRecipeCompleted(selectedRecipe._output));
                        _icon.sprite = _resourceManager.GetResourcePrefab(selectedRecipe._output).GetIcon();
                    }
                }
                else if (_currentlyAvailableRecipes.Count == 0)
                {
                    //:TODO: Bug Reset recipe
                    ResetRecipes();
                    Debug.LogError("RECIPE BUG");
                }

                transportableComponent.gameObject.SetActive(false);

                GameObject.Destroy(transportableComponent.gameObject);
            }
            else
            {
                ResetRecipes();
            }
        }

        private void OnRecipeCompleted(ResourceType recipeOutput)
        {
            _currentState = TransformerState.COOKED;

            _contentList.Clear();
            _audioManager.PlaySound(_audioManager._transformDone);
        }

        private void SetIconVisibility(bool visible)
        {
            _timer.SetVisibility(visible);
        }

        public TransportableComponent GetInstancedContent()
        {
            Recipe currentRecipe = _currentlyAvailableRecipes[0];
            TransportableComponent outputTransportableComponent = _resourceManager.GetResourcePrefab(currentRecipe._output);
            TransportableComponent content = GameObject.Instantiate(outputTransportableComponent);

            content.gameObject.SetActive(true);

            ResetRecipes();

            _emptyTransformer.gameObject.SetActive(_currentState == TransformerState.EMPTY);
            _filledTransformer.gameObject.SetActive(_currentState != TransformerState.EMPTY);

            return content;
        }

        public List<ResourceType> GetContentList()
        {
            return _contentList;
        }

        public TransformerState GetCurrentState()
        {
            return _currentState;
        }
    }
}