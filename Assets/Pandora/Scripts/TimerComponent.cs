using System;
using UnityEngine;
using UnityEngine.UI;

namespace Pandora
{
    public class TimerComponent : MonoBehaviour
    {
        [SerializeField] private Image _radialWipe;

        private float _timerStartValue = 0f;
        private float _currentTime = 0f;
        private Canvas _canvas;
        private Action _onTimerCompletedAction;
        private GameManager _gameManager;

        private void Awake()
        {
            _canvas = GetComponentInChildren<Canvas>();
            SetVisibility(false);
        }

        private void Start()
        {
            _gameManager = GameObject.FindAnyObjectByType<GameManager>();
            _radialWipe.fillAmount = 0f;
        }

        private void Update()
        {
            if (_currentTime > 0f)
            {
                _currentTime -= Time.deltaTime * _gameManager.GetTimeDilation(); //TODO: Multiply by time dilation

                if (_currentTime < 0f)
                {
                    _currentTime = 0f;
                    _radialWipe.fillAmount = 0f;
                    // SetVisibility(false);

                    if (_onTimerCompletedAction != null)
                    {
                        _onTimerCompletedAction();
                    }
                }
                else
                {
                    _radialWipe.fillAmount = _currentTime / _timerStartValue;
                }
            }
        }

        public void StartTimer(float recipeTransformationDuration, Action action)
        {
            _timerStartValue = recipeTransformationDuration;
            _currentTime = recipeTransformationDuration;
            SetVisibility(true);
            _onTimerCompletedAction = action;
        }

        public void SetVisibility(bool visible)
        {
            if (_canvas != null)
            {
                _canvas.enabled = visible;
            }
        }
    }
}
