using Pandora;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleHideObjects : MonoBehaviour
{
    [SerializeField] List<Transform> _objects;

    //private GameManager _gameManager;

    // Start is called before the first frame update
    void Start()
    {
        //_gameManager = GameObject.FindAnyObjectByType<GameManager>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            foreach (Transform t in _objects)
            {
                t.gameObject.SetActive(false);
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            foreach (Transform t in _objects)
            {
                t.gameObject.SetActive(true);
            }
        }
    }
}
