﻿using System.Collections.Generic;

namespace Pandora
{
    public static class GameplayConfig
    {
        public static readonly float TIME_BEFORE_FIRST_CLIENT_SPAWN = 5;

        public static readonly float LONG_PRESS_TIME = 2;

        public static readonly float INITIAL_TIME = 60;
        public static readonly float TIME_INCREMENT = 10;
        public static readonly int INITIAL_MONEY = 25;

        public static readonly int MAX_PLAYER_TRANSPORT_CAPACITY = 4;

        public static readonly float CHARACTER_SPEED = 7.0f;
        public static readonly float CHARACTER_TURN_SPEED = 40.0f;

        public static readonly Dictionary<ResourceType, int> RESOURCE_COSTS = new Dictionary<ResourceType, int>
        {
            {ResourceType.CROISSANT, 20},
            {ResourceType.BREAD, 10},
            {ResourceType.DOUGH, 8},
            {ResourceType.BUTTER, 7},
            {ResourceType.RAW_CROISSANT, 18},
            {ResourceType.FLOUR, 3},
            {ResourceType.WATER, 0},
        };

        public static readonly List<Recipe> RECIPES = new List<Recipe>()
        {
            new Recipe { _inputs = new List<ResourceType>{ResourceType.DOUGH}, _output = ResourceType.BREAD, _transformationDuration = 5 },
            new Recipe { _inputs = new List<ResourceType>{ResourceType.RAW_CROISSANT}, _output = ResourceType.CROISSANT, _transformationDuration = 4 },
            new Recipe { _inputs = new List<ResourceType>{ResourceType.WATER, ResourceType.FLOUR}, _output = ResourceType.DOUGH, _transformationDuration = 2f },
            new Recipe { _inputs = new List<ResourceType>{ResourceType.DOUGH, ResourceType.BUTTER}, _output = ResourceType.RAW_CROISSANT, _transformationDuration = 2f },
        };

        public static readonly List<ResourceType> INITIAL_NEEDS =  new List<ResourceType>
        {
            ResourceType.BREAD,
            ResourceType.BREAD,
            ResourceType.BREAD,
            ResourceType.BREAD,
            ResourceType.BREAD,
            ResourceType.CROISSANT
        };
    }
}