﻿using UnityEngine;

namespace Pandora
{
    public class AudioManager : MonoBehaviour
    {
        public float pitchSemiTones = -12;
        public float _pitchSpeed = 10;

        public AudioSource _musicGoodMood;
        public AudioSource _musicBadMood;

        public AudioSource _customerSpawn;
        public AudioSource _customerUnhappy;
        public AudioSource _customerBuyAction;
        public AudioSource _transformDone;
        public AudioSource _dayCompleted;
        public AudioSource _gameOver;

        public AudioSource _buyWithEnoughMoney;
        public AudioSource _buyWithoutEnoughMoney;
        public AudioSource _action;

        private float _currentPitch = 1;
        private float _targetPitch = 1;
        private float _pitchLerp = 0;

        public void SetMusic(AudioSource audioSource)
        {
            _musicGoodMood.gameObject.SetActive(_musicGoodMood == audioSource );
            _musicBadMood.gameObject.SetActive(_musicBadMood == audioSource );
        }

        public void PlaySound(AudioSource audioSource)
        {
            audioSource.Play();
        }

        public void SetTimeDilatation(float dilation)
        {
            _pitchLerp = 0;
            _targetPitch = dilation == 1 ? 1 : Mathf.Pow(1.05946f, pitchSemiTones);
        }

        private void Update()
        {
            if (_pitchLerp < 1)
            {
                _pitchLerp += Time.deltaTime * _pitchSpeed;
                float pitch = Mathf.Lerp(_currentPitch, _targetPitch, _pitchLerp);
                _musicGoodMood.pitch = pitch;
                _musicBadMood.pitch = pitch;
            }
            else
            {
                _currentPitch = _targetPitch; 
            }
        }
    }
}