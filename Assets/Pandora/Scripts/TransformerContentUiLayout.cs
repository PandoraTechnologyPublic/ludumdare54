﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Pandora
{
    public class TransformerContentUiLayout : MonoBehaviour
    {
        [SerializeField] private TransformationComponent _transformationComponent;
        [SerializeField] private Image _iconPrototype;
        [SerializeField] private RectTransform _iconPanel;

        private ResourceManager _resourceManager;

        private readonly List<Image> _icons = new List<Image>();

        private void Start()
        {
            _resourceManager = GameObject.FindAnyObjectByType<ResourceManager>();

            _iconPrototype.gameObject.SetActive(false);
            _icons.Add(_iconPrototype);
        }

        private void Update()
        {
            int iconIndex = 0;

            List<ResourceType> contentList = _transformationComponent.GetContentList();

            foreach (ResourceType resourceType in contentList)
            {
                if (iconIndex >= _icons.Count)
                {
                    _icons.Add(GameObject.Instantiate(_iconPrototype, _iconPanel));
                }

                _icons[iconIndex].sprite = _resourceManager.GetResourcePrefab(resourceType).GetIcon();
                _icons[iconIndex].gameObject.SetActive(true);

                ++iconIndex;
            }

            for (; iconIndex < _icons.Count; iconIndex++)
            {
                _icons[iconIndex].gameObject.SetActive(false);
            }
        }
    }
}