﻿using UnityEngine;

namespace Pandora
{
    public class StorageComponent : MonoBehaviour
    {
        [SerializeField] private Transform dock;

        private TransportableComponent _content;

        public Vector3 GetDock()
        {
            return dock.transform.position;
        }

        public bool GetFull()
        {
            return _content != null;
        }

        public void SetContent(TransportableComponent content)
        {
            _content = content;
        }

        public object GetContent()
        {
            return _content;
        }
    }
}