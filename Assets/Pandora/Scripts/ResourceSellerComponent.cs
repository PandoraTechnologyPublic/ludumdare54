﻿using TMPro;
using UnityEngine;

namespace Pandora
{
    public class ResourceSellerComponent : MonoBehaviour
    {
        [SerializeField] private ResourceType _resourceType;
        [SerializeField] private TextMeshPro _priceText;

        private void Start()
        {
            _priceText.text = GameplayConfig.RESOURCE_COSTS[_resourceType].ToString();
        }

        public ResourceType GetSoldResource()
        {
            return _resourceType;
        }
    }
}