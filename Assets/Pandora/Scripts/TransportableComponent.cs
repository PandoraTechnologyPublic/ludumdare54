﻿using UnityEngine;

namespace Pandora
{
    public class TransportableComponent : MonoBehaviour
    {
        [SerializeField] private ResourceType _resourceType;
        [SerializeField] private Sprite _icon;

        private StorageComponent _storageComponent;

        public Sprite GetIcon()
        {
            return _icon;
        }

        public ResourceType GetResourceType()
        {
            return _resourceType;
        }

        public void EnableColliders()
        {
            foreach (Collider childCollider in GetComponentsInChildren<Collider>())
            {
                childCollider.enabled = true;
            }
        }

        public void DisableColliders()
        {
            foreach (Collider childCollider in GetComponentsInChildren<Collider>())
            {
                childCollider.enabled = false;
            }
        }

        public void SetContainer(StorageComponent storageComponent)
        {
            _storageComponent = storageComponent;
        }

        public StorageComponent GetContainer()
        {
            return _storageComponent;
        }
    }
}