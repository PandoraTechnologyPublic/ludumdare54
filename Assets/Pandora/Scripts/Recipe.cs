﻿using System.Collections.Generic;

namespace Pandora
{
    public class Recipe
    {
        public List<ResourceType> _inputs;
        public ResourceType _output;
        public float _transformationDuration;
    }
}