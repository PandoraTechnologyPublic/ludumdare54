﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Pandora
{
    public class UpgradeComponent : MonoBehaviour
    {
        [SerializeField] private string _upgradeName;
        [SerializeField] private int _upgradeCost;
        [SerializeField] private List<Transform> _toDeactivate;
        [SerializeField] private List<Transform> _toActivate;

        private void Start()
        {
            foreach (Transform objectToActivate in _toActivate)
            {
                objectToActivate.gameObject.SetActive(false);
            }
        }

        public string GetUpgradeName()
        {
            return _upgradeName;
        }
        public int GetUpgradeCost()
        {
            return _upgradeCost;
        }

        public void Process()
        {
            foreach (Transform objectToDeactivate in _toDeactivate)
            {
                objectToDeactivate.gameObject.SetActive(false);
            }

            foreach (Transform objectToActivate in _toActivate)
            {
                objectToActivate.gameObject.SetActive(true);
            }

            //:TODO: trigger a sound when upgrading and a shake cam?
        }
    }
}