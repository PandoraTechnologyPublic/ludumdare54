﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Pandora
{
    public class UpgradeUiLayout : MonoBehaviour
    {
        [SerializeField] private TMP_Text _nameText;
        [SerializeField] private TMP_Text _costText;
        [SerializeField] private Button _costButton;
        
        private GameManager _gameManager;
        private AudioManager _audioManager;

        private List<UpgradeComponent> _upgradeComponents;
        private UpgradeComponent _upgradeComponent;

        private void Start()
        {
            _audioManager = GameObject.FindAnyObjectByType<AudioManager>();
            _costButton.onClick.AddListener(OnBuyButton);
        }

        private void OnBuyButton()
        {
            _upgradeComponents.Add(_upgradeComponent);
            _gameManager.Pay(_upgradeComponent.GetUpgradeCost());
            _upgradeComponent.Process();
            _costText.SetText("Bought");
            _costButton.interactable = false;
            _audioManager.PlaySound(_audioManager._buyWithEnoughMoney);
            _gameManager.RefreshUpgradeTable();
        }

        public void SetData(UpgradeComponent upgradeComponent, List<UpgradeComponent> upgradeComponents, GameManager gameManager)
        {
            _gameManager = gameManager;
            _upgradeComponents = upgradeComponents;
            _upgradeComponent = upgradeComponent;
            
            _nameText.SetText(upgradeComponent.GetUpgradeName());
            _costText.SetText($"Cost ${upgradeComponent.GetUpgradeCost()}");
            _costButton.interactable = (gameManager.GetPlayerMoney() >= upgradeComponent.GetUpgradeCost()) && !upgradeComponents.Contains(upgradeComponent);
            //
            // if (_costButton.interactable)
            // {
            //     _costButton.Select();
            // }

            if (upgradeComponents.Contains(upgradeComponent))
            {
                _costText.SetText("Bought");
            }
        }

        public Button GetButton()
        {
            return _costButton;
        }
    }
}