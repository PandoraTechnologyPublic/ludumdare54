using Pandora;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Clock : MonoBehaviour
{
    [SerializeField] private float _startTime = 6f;
    [SerializeField] private float _endTime = 12f;
    [SerializeField] private Transform _hourHand;
    [SerializeField] private Transform _minuteHand;
    private GameManager _gameManager;

    private float _currendisplaytTime;
    private float _time;

    // Start is called before the first frame update
    void Start()
    {
        _gameManager = GameObject.FindAnyObjectByType<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        _time = _gameManager.GetDayDuration() - _gameManager.GetTimeLeft();
        float hours = Mathf.Lerp(_startTime, _endTime, Mathf.InverseLerp(0, _gameManager.GetDayDuration(), _time));
        float hourRotation = -360 * (hours/12f);
        _hourHand.transform.localRotation = Quaternion.Euler(0,0, hourRotation);
        _minuteHand.transform.localRotation = Quaternion.Euler(0, 0, hours % 1f * -360);
    }
}
