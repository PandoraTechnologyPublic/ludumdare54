﻿using System.Collections.Generic;
using UnityEditor;
using UnityEditor.Build.Reporting;
using UnityEngine;

namespace Pandora.Editor
{
    public static class BuildScript
    {
        private static readonly bool IS_DEBUG_BUILD = false;

        [MenuItem("Builds/All platforms")]
        private static void BuildAllPlatforms()
        {
            BuildWindowsGame();
            BuildOsxGame();
            BuildLinuxGame();
        }

        [MenuItem("Builds/Windows Build")]
        private static void BuildWindowsGame()
        {
            BuildGame($"/Windows/{Application.productName}.exe", BuildTargetGroup.Standalone, BuildTarget.StandaloneWindows64);
        }

        [MenuItem("Builds/MacOS Build")]
        private static void BuildOsxGame()
        {
            BuildGame($"/MacOS/{Application.productName}.app", BuildTargetGroup.Standalone, BuildTarget.StandaloneOSX);
        }

        [MenuItem("Builds/Linux Build")]
        private static void BuildLinuxGame()
        {
            BuildGame($"/Linux/{Application.productName}.x86_64", BuildTargetGroup.Standalone, BuildTarget.StandaloneLinux64);
        }

        private static void BuildGame(string versionPath, BuildTargetGroup buildTargetGroup, BuildTarget buildTarget)
        {
            BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions
            {
                locationPathName = Application.dataPath + $"/../Versions{versionPath}",
                targetGroup = buildTargetGroup,
                target = buildTarget
            };

            List<string> scenes = new List<string>();

            foreach (EditorBuildSettingsScene editorBuildSettingsScene in EditorBuildSettings.scenes)
            {
                scenes.Add(editorBuildSettingsScene.path);
            }

            buildPlayerOptions.scenes = scenes.ToArray();

            if (IS_DEBUG_BUILD)
            {
                buildPlayerOptions.options = BuildOptions.Development | BuildOptions.ShowBuiltPlayer | BuildOptions.AllowDebugging;
            }
            else
            {
                buildPlayerOptions.options = BuildOptions.ShowBuiltPlayer;
            }

            EditorUserBuildSettings.SwitchActiveBuildTarget(buildPlayerOptions.targetGroup, buildPlayerOptions.target);

            BuildReport buildReport = BuildPipeline.BuildPlayer(buildPlayerOptions);

            Debug.Log(buildReport.summary);
        }
    }
}
