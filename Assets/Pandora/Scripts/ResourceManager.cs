﻿using System.Collections.Generic;
using UnityEngine;

namespace Pandora
{
    public class ResourceManager : MonoBehaviour
    {
        [SerializeField] private List<TransportableComponent> _resourcesPrefabs;

        public TransportableComponent GetResourcePrefab(ResourceType resourceType)
        {
            TransportableComponent selectedResourcePrefab = null;

            foreach (TransportableComponent resourcePrefab in _resourcesPrefabs)
            {
                if (resourcePrefab.GetResourceType() == resourceType)
                {
                    selectedResourcePrefab = resourcePrefab;
                }
            }

            return selectedResourcePrefab;
        }
    }
}