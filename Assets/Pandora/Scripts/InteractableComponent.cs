using System;
using UnityEngine;

namespace Pandora
{
    public class InteractableComponent : MonoBehaviour
    {
        [SerializeField] private Transform _highlight;

        private void Awake()
        {
            HideHighlight();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.TryGetComponent(out SensorComponent sensorComponent))
            {
                sensorComponent.AddInteractable(gameObject.GetComponentInChildren<InteractableComponent>());
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.gameObject.TryGetComponent(out SensorComponent sensorComponent))
            {
                sensorComponent.RemoveInteractable(gameObject.GetComponentInChildren<InteractableComponent>());
            }
        }

        public void ShowHighlight()
        {
            _highlight.gameObject.SetActive(true);
        }

        public void HideHighlight()
        {
            _highlight.gameObject.SetActive(false);
        }

        public void DisableColliders()
        {
            foreach (Collider childCollider in GetComponentsInChildren<Collider>())
            {
                childCollider.enabled = false;
            }
        }
    }
}
