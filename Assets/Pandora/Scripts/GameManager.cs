﻿using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using Unity.AI.Navigation;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Pandora
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private CharacterController _characterController;
        [SerializeField] private CustomerSpawner _customerSpawner;
        [SerializeField] private Transform _counter;
        [SerializeField] private List<Transform> _destinations;
        [SerializeField] private List<UpgradeComponent> _upgrades;
        [SerializeField] private NavMeshSurface _navMesh;

        [Header("Ui")]
        [SerializeField] private TMP_Text _titleText;
        [SerializeField] private TMP_Text _nextButtonText;
        [SerializeField] private TMP_Text _timeText;
        [SerializeField] private TMP_Text _endOfDayPanelTitle;
        [SerializeField] private TMP_Text _endOfDayStatsText;
        [SerializeField] private TMP_Text _moneyText;
        [SerializeField] private TMP_Text _dayText;
        [SerializeField] private TMP_Text _dayIntroText;
        [SerializeField] private RectTransform _endOfDayPanel;
        [SerializeField] private RectTransform _pausePanel;
        [SerializeField] private Image _longPressDisplay;
        [SerializeField] private RectTransform _upgradePanel;
        [SerializeField] private UpgradeUiLayout _upgradePrototype;
        [SerializeField] private Button _nextButton;

        private readonly List<UpgradeComponent> _boughtUpgrades = new List<UpgradeComponent>();
        private readonly List<UpgradeUiLayout> _upgradeUiLayouts = new List<UpgradeUiLayout>();
        private readonly List<CustomerComponent> _sortedCustomer = new List<CustomerComponent>();

        private int _waveCount;
        private int _playerMoney;
        private float _timeLeft;
        private float _dayDuration;
        private float _timeToNextCustomerSpawn;
        private float _dilatedDeltaTime;
        private float _timeDilation = 1f;

        private GameState _currentState = GameState.PLAYING;

        private class GameStats
        {
            public int _satisfiedCustomerCount;
            public int _unsatisfiedCustomerCount;
            public int _moneyEarned;
            public int _moneySpent;
        }

        private float _timeSinceInteractWasPressed;

        private GameStats _lastDayStats = new GameStats();
        private readonly GameStats _gameStats = new GameStats();

        private AudioManager _audioManager;
        private float _saveTimeDilation;
        private List<ResourceType> _customerNeeds;

        private enum GameState
        {
            PLAYING,
            PAUSE,
            END_OF_DAY,
            GAME_OVER,
            START_NEW_DAY
        }

        private void Start()
        {
            _upgradeUiLayouts.Add(_upgradePrototype);
            _upgradePrototype.gameObject.SetActive(false);

            _nextButton.onClick.AddListener(OnGoToNextDayButton);

            _pausePanel.gameObject.SetActive(false);
            _endOfDayPanel.gameObject.SetActive(false);
            _longPressDisplay.gameObject.SetActive(false);

            _audioManager = GameObject.FindAnyObjectByType<AudioManager>();

            AddMoney(GameplayConfig.INITIAL_MONEY);
            SetTime(GameplayConfig.INITIAL_TIME);

            _customerNeeds = GameplayConfig.INITIAL_NEEDS;

            _timeToNextCustomerSpawn = GameplayConfig.TIME_BEFORE_FIRST_CLIENT_SPAWN;

            _audioManager.SetMusic(_audioManager._musicGoodMood);

            StartIntroTextAnimation();
        }

        private void StartIntroTextAnimation()
        {
            _dayIntroText.SetText($"Day {_waveCount + 1} Starts");
            _dayIntroText.gameObject.SetActive(false);
            _dayIntroText.transform.localScale = Vector3.zero;
            _dayIntroText.alpha = 1;
            _dayIntroText.gameObject.SetActive(true);
            DOTween.Sequence()
                .Append(_dayIntroText.transform.DOScale(Vector3.one, 2.0f).SetEase(Ease.OutBounce))
                .Append(_dayIntroText.DOFade(0, 0.6f))
                .AppendCallback(() => _dayIntroText.gameObject.SetActive(false));
        }

        private void OnGoToNextDayButton()
        {
            switch (_currentState)
            {
                case GameState.END_OF_DAY:
                {
                    // if (HasLongPressed())
                    {
                        ChangeState(GameState.START_NEW_DAY);
                        _navMesh.BuildNavMesh();
                    }

                    break;
                }
                case GameState.GAME_OVER:
                {
                    // if (HasLongPressed())
                    {
                        SceneManager.LoadScene(0);
                    }

                    break;
                }
            }
        }

        private void Update()
        {
            switch (_currentState)
            {
                case GameState.PLAYING:
                {
                    _dilatedDeltaTime = Time.deltaTime * _timeDilation;

                    _timeLeft = Mathf.Max(0, _timeLeft - _dilatedDeltaTime);
                    _timeToNextCustomerSpawn -= _dilatedDeltaTime;

                    _timeText.SetText($"{Mathf.Floor(_timeLeft / 60.0f):00}:{Mathf.Floor(_timeLeft % 60):00}");

                    if ((_timeLeft > 0) && (_timeToNextCustomerSpawn <= 0))
                    {
                        _sortedCustomer.Add(_customerSpawner.SpawnNewCustomer(_customerNeeds[Random.Range(0, _customerNeeds.Count)]));
                        _audioManager.PlaySound(_audioManager._customerSpawn);

                        float elapsedDayPercentage = _timeLeft / _dayDuration;
                        int dayTimeSlotIndex = Mathf.FloorToInt(elapsedDayPercentage / 0.20f);

                        if (dayTimeSlotIndex % 2 == 0)
                        {
                            _timeToNextCustomerSpawn = Random.Range(10, 14);
                        }
                        else
                        {
                            _timeToNextCustomerSpawn = Random.Range(4, 7);
                        }
                    }

                    if (_sortedCustomer.Count > 0)
                    {
                        _sortedCustomer[0].GoToCounter(_counter);
                    }
                    else if (_timeLeft <= 0)
                    {
                        ChangeState(GameState.END_OF_DAY);
                    }

                    InputAction menuInputAction = _characterController.GetPlayerInput().actions["Menu"];

                    if (menuInputAction.WasReleasedThisFrame())
                    {
                        menuInputAction.Reset();
                        ChangeState(GameState.PAUSE);
                    }

                    break;
                }
                case GameState.PAUSE:
                {
                    InputAction menuInputAction = _characterController.GetPlayerInput().actions["Menu"];

                    if (menuInputAction.WasReleasedThisFrame())
                    {
                        menuInputAction.Reset();
                        _pausePanel.gameObject.SetActive(false);
                        ChangeState(GameState.PLAYING);
                    }

                    break;
                }
                // case GameState.END_OF_DAY:
                // {
                //     if (HasLongPressed())
                //     {
                //         ChangeState(GameState.START_NEW_DAY);
                //         _navMesh.BuildNavMesh();
                //     }
                //
                //     break;
                // }
                // case GameState.GAME_OVER:
                // {
                //     if (HasLongPressed())
                //     {
                //         SceneManager.LoadScene(0);
                //     }
                //
                //     break;
                // }
            }
        }

        private bool HasLongPressed()
        {
            bool hasLongPressed = false;

            InputAction inputAction = _characterController.GetPlayerInput().actions["Interact"];

            if (inputAction.WasPressedThisFrame())
            {
                _timeSinceInteractWasPressed = 0;
                _longPressDisplay.gameObject.SetActive(true);
            }
            if (inputAction.IsPressed())
            {
                _timeSinceInteractWasPressed += Time.deltaTime;

                _longPressDisplay.fillAmount = _timeSinceInteractWasPressed / GameplayConfig.LONG_PRESS_TIME;

                if (_timeSinceInteractWasPressed >= GameplayConfig.LONG_PRESS_TIME)
                {
                    hasLongPressed = true;
                }
            }

            if (inputAction.WasReleasedThisFrame())
            {
                _longPressDisplay.gameObject.SetActive(false);
            }

            return hasLongPressed;
        }

        private void ChangeState(GameState nextState)
        {
            _currentState = nextState;

            switch (_currentState)
            {
                case GameState.PAUSE:
                {
                    _pausePanel.gameObject.SetActive(true);

                    _saveTimeDilation = _timeDilation;
                    _timeDilation = 0;
                    // Time.timeScale = 0;

                    break;
                }
                case GameState.GAME_OVER:
                {
                    _titleText.SetText("You ran out of business");
                    _nextButtonText.SetText("Make a new attempt");

                    _endOfDayPanel.gameObject.SetActive(true);
                    _endOfDayPanelTitle.SetText("Game stats");
                    _endOfDayStatsText.SetText($"Satisfied customer(s): {_gameStats._satisfiedCustomerCount}\n" +
                       $"Unsatisfied customer(s): {_gameStats._unsatisfiedCustomerCount}\n" +
                       $"Money earned: {_gameStats._moneyEarned}\n" +
                       $"Money spent: {_gameStats._moneySpent}\n" +
                       $"Day survived: {_waveCount}"
                    );

                    _upgradePanel.gameObject.SetActive(false);

                    _nextButton.Select();

                    break;
                }
                case GameState.END_OF_DAY:
                {
                    _titleText.SetText($"Day {_waveCount + 1} complete: Well done!");
                    _nextButtonText.SetText("Continue to the Next day");

                    if (_playerMoney < 0)
                    {
                        _audioManager.PlaySound(_audioManager._gameOver);
                        ChangeState(GameState.GAME_OVER);
                    }
                    else
                    {
                        _audioManager.PlaySound(_audioManager._dayCompleted);

                        _endOfDayPanel.gameObject.SetActive(true);
                        _endOfDayPanelTitle.SetText("Day stats");
                        _endOfDayStatsText.SetText($"Satisfied customer(s): {_lastDayStats._satisfiedCustomerCount}\n" +
                                                   $"Unsatisfied customer(s): {_lastDayStats._unsatisfiedCustomerCount}\n" +
                                                   $"Money earned: {_lastDayStats._moneyEarned}\n" +
                                                   $"Money spent: {_lastDayStats._moneySpent}"
                        );

                        RefreshUpgradeTable();
                    }

                    _saveTimeDilation = _timeDilation;

                    break;
                }
                case GameState.START_NEW_DAY:
                {
                    _lastDayStats = new GameStats();
                    _endOfDayPanel.gameObject.SetActive(false);
                    //:TODO: trigger next phase
                    ++_waveCount;
                    _dayText.SetText($"Day {_waveCount + 1}");
                    SetTime(GameplayConfig.INITIAL_TIME + GameplayConfig.TIME_INCREMENT * _waveCount);

                    _customerNeeds.Add(ResourceType.BREAD);
                    _customerNeeds.Add(ResourceType.CROISSANT);

                    _timeToNextCustomerSpawn = 0;
                    ChangeState(GameState.PLAYING);

                    StartIntroTextAnimation();

                    break;
                }
                case GameState.PLAYING:
                {
                    Time.timeScale = 1;
                    _timeDilation = _saveTimeDilation;

                    break;
                }
            }
        }

        public void RefreshUpgradeTable()
        {
            Navigation nextButtonNavigation = _nextButton.navigation;

            nextButtonNavigation.selectOnDown = null;
            nextButtonNavigation.selectOnUp = null;

            _upgradePanel.gameObject.SetActive(_upgrades.Count > 0);

            Button previousButton = null;

            for (int upgradeIndex = 0; upgradeIndex < _upgrades.Count; upgradeIndex++)
            {
                UpgradeComponent upgradeComponent = _upgrades[upgradeIndex];

                if (upgradeIndex >= _upgradeUiLayouts.Count)
                {
                    UpgradeUiLayout upgradeUiLayout = GameObject.Instantiate(_upgradePrototype, _upgradePanel);

                    _upgradeUiLayouts.Add(upgradeUiLayout);
                }

                _upgradeUiLayouts[upgradeIndex].SetData(upgradeComponent, _boughtUpgrades, this);
                _upgradeUiLayouts[upgradeIndex].gameObject.SetActive(true);

                Button button = _upgradeUiLayouts[upgradeIndex].GetButton();

                if (button.interactable)
                {
                    Navigation buttonNavigation = button.navigation;

                    buttonNavigation.selectOnUp = null;
                    buttonNavigation.selectOnDown = null;

                    if (previousButton == null)
                    {
                        buttonNavigation.selectOnUp = _nextButton;

                        nextButtonNavigation.selectOnDown = button;
                    }
                    else
                    {
                        buttonNavigation.selectOnUp = previousButton;

                        Navigation previousButtonNavigation = previousButton.navigation;
                        previousButtonNavigation.selectOnDown = button;
                        previousButton.navigation = previousButtonNavigation;

                    }

                    button.navigation = buttonNavigation;

                    previousButton = button;
                }
            }

            nextButtonNavigation.selectOnUp = previousButton;
            _nextButton.navigation = nextButtonNavigation;

            _nextButton.Select();
        }

        public float GetTimeLeft()
        { 
            return _timeLeft;
        }

        public float GetDayDuration()
        {
            return _dayDuration;
        }

        public float GetTimeDilation()
        {
            return _timeDilation;
        }

        public void SetTimeDilation(float dilation)
        {
            _timeDilation = dilation;
            _audioManager.SetTimeDilatation(dilation);
        }

        private void SetTime(float time)
        {
            _timeLeft += time;
            _dayDuration = time;

            _timeText.SetText($"{Mathf.Floor(_timeLeft / 60.0f):##}:{_timeLeft % 60:##}");
        }

        public void AddMoney(int gainedMoney)
        {
            if ((_playerMoney < 0) && (_playerMoney + gainedMoney >= 0))
            {
                _audioManager.SetMusic(_audioManager._musicGoodMood);
            }

            _playerMoney += gainedMoney;
            _lastDayStats._moneyEarned += gainedMoney;
            _gameStats._moneyEarned += gainedMoney;
            _moneyText.SetText($"$$ {_playerMoney}");
        }

        public int GetPlayerMoney()
        {
            return _playerMoney;
        }

        public void Pay(int cost)
        {
            if ((_playerMoney >= 0) && (_playerMoney - cost < 0))
            {
                _audioManager.SetMusic(_audioManager._musicBadMood);
            }

            _playerMoney -= cost;
            _lastDayStats._moneySpent += cost;
            _gameStats._moneySpent += cost;

            _moneyText.SetText($"$$ {_playerMoney}");
        }

        public Transform GetRandomDestination()
        {
            return _destinations[Random.Range(0, _destinations.Count)];
        }

        public void QuitBakery(CustomerComponent customerComponent)
        {
            InteractableComponent interactableComponent = customerComponent.GetComponent<InteractableComponent>();
            interactableComponent.DisableColliders();
            _characterController.GetComponentInChildren<SensorComponent>().RemoveInteractable(interactableComponent);
            _sortedCustomer.Remove(customerComponent);
            //customerComponent.GoToExit();
            GameObject.Destroy(customerComponent.gameObject);
        }
        public void AddSatisfiedCustomer()
        {
            ++_lastDayStats._satisfiedCustomerCount;
            ++_gameStats._satisfiedCustomerCount;
        }

        public void AddUnsatisfiedCustomer()
        {
            ++_lastDayStats._unsatisfiedCustomerCount;
            ++_gameStats._unsatisfiedCustomerCount;
        }

        public bool IsPlaying()
        {
            return _currentState == GameState.PLAYING;
        }
    }
}