﻿using UnityEngine;

namespace Pandora
{
    public class CustomerSpawner : MonoBehaviour
    {
        [SerializeField] private CustomerComponent _customerPrefab;

        public CustomerComponent SpawnNewCustomer(ResourceType customerNeed)
        {
            CustomerComponent instancedCustomer = GameObject.Instantiate(_customerPrefab, transform.position, Quaternion.identity);

            instancedCustomer.SetNeed(customerNeed);

            return instancedCustomer;
        }
    }
}