using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.InputSystem;

namespace Pandora
{
    public class CharacterController : MonoBehaviour
    {
        [SerializeField] private Transform _rotationNode;
        [SerializeField] private List<Transform> _transportDocks;
        [SerializeField] private float _deadZone = 0.2f;

        private PlayerInput _playerInput;
        private Camera _playerCamera;
        private Vector3 _facingDirection;
        private NavMeshAgent _navMeshAgent;
        private Animator _animator;

        private GameManager _gameManager;

        // private TransportableComponent _transportedElement;
        private readonly List<TransportableComponent> _transportedElements = new List<TransportableComponent>();

        private void Start()
        {
            _playerInput = GetComponent<PlayerInput>();
            _navMeshAgent = GetComponent<NavMeshAgent>();
            _playerCamera = GetComponentInChildren<Camera>();
            _gameManager = GameObject.FindAnyObjectByType<GameManager>();
            _animator = GetComponentInChildren<Animator>();
        }

        private void Update()
        {
            if (_gameManager.IsPlaying())
            {
                Vector2 readValue = _playerInput.actions["Move"].ReadValue<Vector2>();

                if (readValue.sqrMagnitude >= _deadZone)
                {
                    Vector3 movementDirection = new Vector3(readValue.x, 0, readValue.y);

                    movementDirection = _playerCamera.transform.TransformDirection(movementDirection);

                    movementDirection.y = 0;

                    _facingDirection = movementDirection.normalized;
                    if ((_rotationNode.transform.forward + _facingDirection).sqrMagnitude < 0.01f)
                    {
                        _facingDirection.x += 0.01f;
                    }

                    _rotationNode.transform.forward = Vector3.Lerp(_rotationNode.transform.forward, _facingDirection, Time.deltaTime * GameplayConfig.CHARACTER_TURN_SPEED);
                    // LookAt(_rotationNode.transform.position + _facingDirection);

                    _navMeshAgent.Move(_facingDirection * (readValue.magnitude * (Time.deltaTime * GameplayConfig.CHARACTER_SPEED)));
                    //transform.Translate(_facingDirection * (readValue.magnitude * (Time.deltaTime * GameplayConfig.CHARACTER_SPEED)));

                    _animator.SetBool("walking", true);
                }
                else 
                {
                    _animator.SetBool("walking", false);
                }
            }
        }

        public PlayerInput GetPlayerInput()
        {
            return _playerInput;
        }

        public bool CanPickUpMoreStuff()
        {
            return _transportedElements.Count < GameplayConfig.MAX_PLAYER_TRANSPORT_CAPACITY;
        }

        public TransportableComponent GetLastPickedUpElement()
        {
            TransportableComponent lastTransportedElement = null;

            if (_transportedElements.Count > 0)
            {
                lastTransportedElement = _transportedElements[_transportedElements.Count - 1];
            }

            return lastTransportedElement;
        }

        public TransportableComponent Drop(Vector3 position)
        {
            TransportableComponent elementToDrop = _transportedElements[_transportedElements.Count - 1];

            elementToDrop.transform.position = position + Vector3.up; //:TODO: can be the wrong spot if we are not using dock
            elementToDrop.transform.SetParent(null); //:TODO: parent to storage ?
            elementToDrop.gameObject.SetActive(true);

            _transportedElements.RemoveAt(_transportedElements.Count - 1);

            if (_transportedElements.Count == 0)
            {
                _animator.SetBool("holding", false);
            }

            return elementToDrop;
        }

        public void PickUp(TransportableComponent transportableComponent)
        {
            _transportedElements.Add(transportableComponent);

            Transform transform = transportableComponent.transform;

            transform.SetParent(_transportDocks[_transportedElements.Count - 1]);
            transform.localRotation = Quaternion.identity;
            transform.localPosition = Vector3.zero;

            transportableComponent.DisableColliders();

            InteractableComponent interactableComponent = transportableComponent.GetComponent<InteractableComponent>();
            GetComponentInChildren<SensorComponent>().RemoveInteractable(interactableComponent);

            _animator.SetBool("holding", true);
        }
    }
}
