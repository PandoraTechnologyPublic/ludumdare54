﻿using UnityEngine;

namespace Pandora
{
    public class MeshRotate : MonoBehaviour
    {
        private void FixedUpdate()
        {
            transform.Rotate(Vector3.up, 6.0f * Time.fixedDeltaTime,Space.Self);
        }
    }
}