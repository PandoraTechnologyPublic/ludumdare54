﻿using System.Collections.Generic;
using UnityEngine;

namespace Pandora
{
    public class SensorComponent : MonoBehaviour
    {
        private CharacterController _characterController;
        private ResourceManager _resourceManager;
        private GameManager _gameManager;
        private AudioManager _audioManager;

        private readonly List<InteractableComponent> _highlightableTriggers = new List<InteractableComponent>();

        private void Start()
        {
            _characterController = gameObject.GetComponentInParent<CharacterController>();
            _gameManager = GameObject.FindAnyObjectByType<GameManager>();
            _audioManager = GameObject.FindAnyObjectByType<AudioManager>();
            _resourceManager = GameObject.FindAnyObjectByType<ResourceManager>();
        }

        private void FixedUpdate()
        {
            InteractableComponent selectedHighlightedTrigger = null;
            TransportableComponent lastPickedUpElement = _characterController.GetLastPickedUpElement();
            bool canPickUpMoreStuff = _characterController.CanPickUpMoreStuff();

            foreach (InteractableComponent highlightedTrigger in _highlightableTriggers)
            {
                highlightedTrigger.HideHighlight();

                if (lastPickedUpElement != null)
                {
                    if (highlightedTrigger.TryGetComponent(out TransformationComponent transformationComponent)
                        && new List<TransformationComponent.TransformerState> { TransformationComponent.TransformerState.EMPTY, TransformationComponent.TransformerState.WAITING_INGREDIENTS}.Contains(transformationComponent.GetCurrentState())
                        && transformationComponent.DoesRequireIngredient(lastPickedUpElement.GetResourceType())
                        )
                    {
                        selectedHighlightedTrigger = highlightedTrigger;
                    }
                    else if (highlightedTrigger.TryGetComponent(out CustomerComponent customerComponent))
                    {
                        if (customerComponent.GetNeedType() == lastPickedUpElement.GetResourceType())
                        {
                            selectedHighlightedTrigger = highlightedTrigger;
                        }
                    }
                    else if (highlightedTrigger.TryGetComponent(out StorageComponent storageComponent) && (storageComponent.GetContent() == null))
                    {
                        selectedHighlightedTrigger = highlightedTrigger;
                    }
                    else if (highlightedTrigger.TryGetComponent(out ResourceSellerComponent resourceSellerComponent))
                    {
                        selectedHighlightedTrigger = highlightedTrigger;
                    }
                }

                if (canPickUpMoreStuff)
                {
                    if (highlightedTrigger.TryGetComponent(out TransportableComponent transportableComponent))
                    {
                        selectedHighlightedTrigger = highlightedTrigger;
                    }
                    else if (highlightedTrigger.TryGetComponent(out TransformationComponent transformationComponent) &&
                        (
                            (transformationComponent.GetCurrentState() == TransformationComponent.TransformerState.COOKED)
                            //|| (transformationComponent.GetCurrentState() == TransformationComponent.TransformerState.WAITING_INGREDIENTS) 
                            //Tentative avortée de pouvoir récupérer pour pas être coinçé...
                        ))
                    {
                        selectedHighlightedTrigger = highlightedTrigger;
                    }
                    else if (highlightedTrigger.TryGetComponent(out ResourceSellerComponent resourceSellerComponent))
                    {
                        selectedHighlightedTrigger = highlightedTrigger;
                    }
                }
            }

            bool shouldTriggerActionSound = false;

            if (selectedHighlightedTrigger != null)
            {
                selectedHighlightedTrigger.ShowHighlight();

                if (_gameManager.IsPlaying() && _characterController.GetPlayerInput().actions["Interact"].WasReleasedThisFrame())
                {
                    if (selectedHighlightedTrigger.TryGetComponent(out TransformationComponent transformationComponent))
                    {
                        if (canPickUpMoreStuff && (transformationComponent.GetCurrentState() == TransformationComponent.TransformerState.COOKED))
                        {
                            TransportableComponent content = transformationComponent.GetInstancedContent();

                            if (content != null)
                            {
                                _characterController.PickUp(content);
                                content.transform.localPosition = Vector3.zero;
                                content.gameObject.SetActive(true);

                                shouldTriggerActionSound = true;
                            }
                        }
                        else if ((lastPickedUpElement != null) && new List<TransformationComponent.TransformerState> { TransformationComponent.TransformerState.EMPTY, TransformationComponent.TransformerState.WAITING_INGREDIENTS}.Contains(transformationComponent.GetCurrentState()))
                        {
                            TransportableComponent droppedElement = _characterController.Drop(transformationComponent.transform.position);
                            droppedElement.DisableColliders();
                            droppedElement.gameObject.SetActive(false);

                            transformationComponent.AddContent(droppedElement);

                            shouldTriggerActionSound = true;
                        }
                    }
                    else if (selectedHighlightedTrigger.TryGetComponent(out CustomerComponent customerComponent))
                    {
                        if (lastPickedUpElement != null)
                        {
                            TransportableComponent droppedElement = _characterController.Drop(lastPickedUpElement.transform.position);

                            droppedElement.DisableColliders();
                            droppedElement.gameObject.SetActive(false);

                            customerComponent.Buy(droppedElement.GetResourceType());

                            RemoveInteractable(droppedElement.GetComponent<InteractableComponent>());
                            RemoveInteractable(customerComponent.GetComponent<InteractableComponent>());

                            GameObject.Destroy(droppedElement.gameObject);

                            _audioManager.PlaySound(_audioManager._customerBuyAction);
                        }
                    }
                    else if (selectedHighlightedTrigger.TryGetComponent(out StorageComponent storageComponent))
                    {
                        if ((lastPickedUpElement != null) && (storageComponent.GetContent() == null))
                        {
                            // storageComponent.SetFull(true); WIP!
                            TransportableComponent droppedElement = _characterController.Drop(storageComponent.GetDock());
                            storageComponent.SetContent(droppedElement);
                            droppedElement.EnableColliders();
                            droppedElement.SetContainer(storageComponent);

                            shouldTriggerActionSound = true;
                        }
                    }
                    else if (selectedHighlightedTrigger.TryGetComponent(out TransportableComponent transportableComponent))
                    {
                        if (canPickUpMoreStuff)
                        {
                            _characterController.PickUp(transportableComponent);
                            transportableComponent.GetContainer().SetContent(null);
                            transportableComponent.SetContainer(null);

                            shouldTriggerActionSound = true;
                        }
                    }
                    else if (selectedHighlightedTrigger.TryGetComponent(out ResourceSellerComponent resourceSellerComponent))
                    {
                        if (canPickUpMoreStuff)
                        {
                            int resourceCost = GameplayConfig.RESOURCE_COSTS[resourceSellerComponent.GetSoldResource()];

                            //if (resourceCost <= _gameManager.GetPlayerMoney())
                            {
                                TransportableComponent outputTransportableComponent = _resourceManager.GetResourcePrefab(resourceSellerComponent.GetSoldResource());

                                TransportableComponent boughtResource = GameObject.Instantiate(outputTransportableComponent);

                                if (resourceCost > _gameManager.GetPlayerMoney())
                                {
                                    _audioManager.PlaySound(_audioManager._buyWithEnoughMoney);
                                    //:TODO: trigger an weird  sound
                                }
                                else
                                {
                                    _audioManager.PlaySound(_audioManager._buyWithoutEnoughMoney);
                                }

                                _gameManager.Pay(resourceCost);
                                //boughtResource.GetComponent<InteractableComponent>().enabled = false;
                                _characterController.PickUp(boughtResource);
                                boughtResource.transform.localPosition = Vector3.zero;
                                boughtResource.gameObject.SetActive(true);
                            }
                        }
                    }
                }
            }

            if (shouldTriggerActionSound)
            {
                _audioManager.PlaySound(_audioManager._action);
            }
        }

        public void AddInteractable(InteractableComponent interactableComponent)
        {
            _highlightableTriggers.Add(interactableComponent);
        }

        public void RemoveInteractable(InteractableComponent interactableComponent)
        {
            interactableComponent.HideHighlight();
            _highlightableTriggers.Remove(interactableComponent);
        }
    }
}