using Pandora;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SunLightCycle : MonoBehaviour
{
    [SerializeField] private Quaternion _startRotation;
    [SerializeField] private Quaternion _endRotation;
    [SerializeField] private Color _startColor;
    [SerializeField] private Color _endColor;

    private Light _light;
    private GameManager _gameManager;

    private float _time;

    // Start is called before the first frame update
    void Start()
    {
        _gameManager = GameObject.FindAnyObjectByType<GameManager>();
        _light = GetComponent<Light>();
    }

    // Update is called once per frame
    void Update()
    {
        _time = _gameManager.GetDayDuration() - _gameManager.GetTimeLeft();
        float t = Mathf.InverseLerp(0, _gameManager.GetDayDuration(), _time);
        transform.rotation = Quaternion.Lerp(_startRotation, _endRotation, t);
        _light.color = Color.Lerp(_startColor, _endColor, t);
    }
}
